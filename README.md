# Eigen library for Arduino Due 

---------------------------------------------------------------------------------------

This is a updated version of this library. 
-> https://github.com/mcprakash/Eigen (3.1.3)
to Eigen version 3.3.7

---------------------------------------------------------------------------------------

To update this library is simple. 

Extract the library and download the eigen release.

In the arduino library folder, remove 'Eigen' folder.

In the eigen library folder, copy 'Eigen' folder to arduino library folder.

And that's all.

---------------------------------------------------------------------------------------

To install, simply clone or download this library into your Arduino/libraries folder. 

---------------------------------------------------------------------------------------

To see eigen version of the library, in the root library folder:

The version number macros are defined in Macros.h located at \Eigen\src\Core\util\.

---------------------------------------------------------------------------------------

Please see [Eigen's website](https://eigen.tuxfamily.org/dox/index.html) for documentation.
